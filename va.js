function triggerBot(input) {
    let bot = document.getElementById('bot');
    bot.dataset.type = input;
    bot.classList.remove('active');
    fetch('https://VVAA-VVAA.cxcompany.com/event/' + input + '?apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl')
    .then(response => { response.json().then(
        function(data) {
            let sessionId = data.sessionId;
            let eventResponse = data.outputs[0].outputParts[0];
            let responseStrings = eventResponse.text.split(/\r?\n/);
            let dialogPath = data.outputs[0].dialogPath;
            let htmlObject = '';
            for (var i = 0; i < responseStrings.length; i++) {
                let string = responseStrings[i];
                if (string !== "  " && string !== "" && !string.includes('DialogOption')) {
                    let htmlString = '<p>' + string + '</p>';
                    htmlObject = htmlObject.concat(htmlString);
                    eventResponse.text = htmlObject;
                }
            }
            let conversation = [];
            conversation.push(eventResponse);
            localStorage.setItem("conversation", JSON.stringify(conversation));
            localStorage.setItem("sessionid", sessionId);
            localStorage.setItem("bot", input);
            let hasOptions = '';
            let dialogOptions;
            if (eventResponse.userAnswer) {
                userAnswer = 'userAnswer';
            }
            if (eventResponse.dialogOptions) {
                if (eventResponse.dialogOptions.length) {
                    hasOptions = 'options';
                }
            }

            let optionsHtml = '';
            if (eventResponse.dialogOptions) {
                if (eventResponse.dialogOptions.length) {
                    for (var i = 0; i < eventResponse.dialogOptions.length; i++) {
                        let option = eventResponse.dialogOptions[i];
                        let dataOption = option.toLowerCase();
                        dataOption = dataOption.split(' ').join('_');
                        let optionHtml = '<span class="conv_option" data-option="' + dataOption + '" onclick="selectOption(\'' + option + '\', \'' + dialogPath + '\', this)">' + option + '</span>'
                        optionsHtml += optionHtml;
                    }
                }
            }

            let messageHtml =
                '<div class="conv_message ' + hasOptions + '">' +
                '<div>' + eventResponse.text + '</div>' +
                '<p class="conv_options">' +
                    optionsHtml +
                '</p>'+
            '</div>';
            const botBody = document.getElementById('bot_body');
            botBody.innerHTML = '<div id="waiting-element">' +
                '<p>' +
                    '<img src="https://www.vvaa.nl/-/media/service/icons/typing.svg" />' +
                '</p>' +
            '</div>';
            botBody.insertAdjacentHTML('afterbegin', messageHtml);
            let bot = document.getElementById('bot');
            bot.classList.add('active');
        });
    });
}

document.addEventListener("DOMContentLoaded", function() {
    let bot = document.getElementById('bot');
    let botHead = document.getElementById('bot_header');
    botHead.addEventListener("click", function() {
        if (bot.classList.contains('active-open')) {
            bot.classList.remove('active-open');
            bot.classList.add('active');
        }
        else {
            bot.classList.add('active-open');
            bot.classList.remove('active');
        }
    });
});




function selectOption(dialogOption, dialogpath, e) {
    // console.log(e);
    //
    // // Revert and change choice
    // let optionsEl = e.closest('.conv_options');
    // let revertEl = '<span class="revert-choice" onclick="revertChoice(\'' + dialogpath + '\', this)">Kies opnieuw</span>';
    // if (optionsEl) {
    //     optionsEl.insertAdjacentHTML('afterend', revertEl);
    //     let optionsHtml = optionsEl.innerHTML;
    //     let optionsWrapperHtml = '<p class="conv-options">' + optionsHtml + '</p>';
    //     let revertElHtml = document.querySelector('.revert-choice');
    //     revertElHtml.insertAdjacentHTML('afterend', optionsWrapperHtml);
    // }
    let sessionId = localStorage.getItem("sessionid");
    let dataDialogOption = dialogOption.toLowerCase();
    dataDialogOption = dataDialogOption.split(' ').join('_');
    let dialogOptionItems = document.querySelectorAll('.conv_option');
    dialogOptionItems.forEach(function(el) {
        el.classList.add('noclick');
        if (el.dataset.option === dataDialogOption) {
            el.classList.add('active');
        }
    })
    let waitingElement = document.getElementById('waiting-element');
    waitingElement.style.display = 'block';
    let dialogOptionObject = '<p>' + dialogOption + '</p>';
    let conversationObject = {
        userAnswer: true,
        text: dialogOptionObject
    }
    let conversation = JSON.parse(localStorage.getItem("conversation"));
    conversation.push(conversationObject);
    localStorage.setItem("conversation", JSON.stringify(conversation));
    let messageHtml =
        '<div class="conv_message userAnswer">' +
        '<div><p>' + dialogOption + '</p></div>' +
    '</div>';
    let items = document.querySelectorAll('.conv_message');
    let lastItem = items[items.length- 1];
    lastItem.insertAdjacentHTML('afterend', messageHtml);
    toBottom();
    let dialogOptionToLowerCase = dialogOption.toLowerCase();
    if (dialogOptionToLowerCase === 'mijn zorgverzekering' ||
    dialogOptionToLowerCase === 'een andere zorgverzekering' ||
    dialogOptionToLowerCase === 'mijn persoonsgegevens' ||
    dialogOptionToLowerCase === 'een intermediair product' ||
    dialogOptionToLowerCase === 'autoschade' ||
    dialogOptionToLowerCase === 'reisschade' ||
    dialogOptionToLowerCase === 'aansprakelijkheid' ||
    dialogOptionToLowerCase === 'andere schade' ||
    dialogOptionToLowerCase === 'factuur betalen' ||
    dialogOptionToLowerCase === 'betalingsregeling' ||
    dialogOptionToLowerCase === 'uitstel van betaling') {
        localStorage.setItem('subcategory', dialogOptionToLowerCase);
    }
    dialogOption = encodeURI(dialogOption);
    fetch('https://VVAA-VVAA.cxcompany.com/ask?apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl&session.id='+sessionId+'&q='+dialogOption)
    .then(response => { response.json().then(
        function(data) {
            let responseId = data.outputs[0].outputId;
            let eventResponse = data.outputs[0].outputParts[0];
            console.log(data);
            let interactionId = data.interactionId;
            let dialogPath = data.outputs[0].dialogPath;
            linksAndImagesVA(eventResponse, interactionId);
            let responseStrings = eventResponse.text.split(/\r?\n/);
            let htmlObject = '';
            let bubbleAmount;
            let cleanResponseStrings = [];
            let bubbles = [];
            for (var i = 0; i < responseStrings.length; i++) {
                let string = responseStrings[i];
                if (string !== "  " && string !== "" && !string.includes('DialogOption')) {
                    string = marked(string);
                    string = string.replace('<ul>', '');
                    string = string.replace('</ul>', '');
                    bubbleAmount = i;
                    cleanResponseStrings.push(string);
                }
            }
            for (var i = 0; i < cleanResponseStrings.length; i++) {
                let string = cleanResponseStrings[i];
                if (string.includes('<u>')) {
                    string = string.replace('<u>', '<u onclick="liveChat(\'VA\')">')
                }
                bubbles.push(string);
            }
            let optionsHtml = '';
            let hasOptions = '';
            if (eventResponse.dialogOptions) {
                if (eventResponse.dialogOptions.length) {
                    hasOptions = 'options';
                    for (var i = 0; i < eventResponse.dialogOptions.length; i++) {
                        let option = eventResponse.dialogOptions[i];
                        if (option.includes('contact')) {
                            hasOptions = 'options fullfilled'
                        }
                        let dataOption = option.toLowerCase();
                        dataOption = dataOption.split(' ').join('_');
                        let optionHtml = '<span class="conv_option" data-option="' + dataOption + '" onclick="selectOption(\'' + option + '\', \'' + dialogPath + '\', this)">' + option + '</span>'
                        optionsHtml += optionHtml;
                    }
                }
            }
            let optionsWrapperHtml =
                '<p class="conv_options">' +
                    optionsHtml +
                '</p>';
            let messageWrapperHtml =
                '<div class="conv_message ' + hasOptions + '">' +
                    '<div>' + bubbles[0] + '</div>' +
                '</div>';
            let items = document.querySelectorAll('.conv_message');
            let lastItem = items[items.length- 1];
            let conversation = JSON.parse(localStorage.getItem("conversation"));
            setTimeout(function () {
                conversation.push(conversationObject);
                lastItem.insertAdjacentHTML('afterend', messageWrapperHtml);
                toBottom();
                let waitingElement = document.getElementById('waiting-element');
                if (bubbles.length === 1) {
                    waitingElement.style.display = 'none';
                    let items = document.querySelectorAll('.conv_message');
                    let lastItem = items[items.length- 1];
                    if (eventResponse.dialogOptions) {
                        if (eventResponse.dialogOptions.length) {
                            lastItem.insertAdjacentHTML('beforeend', optionsWrapperHtml);
                            toBottom();
                        }
                        if (data.outputs[0].dialogPath.includes('!')) {
                            let botType = document.getElementById('bot').dataset.type;
                            let escalationHtml = '<div class="conv_message"><div><p>Bent u zo geholpen?</p></div></div><br />' +
                            '<p class="conv_options" id="fulfillment">' +
                                '<span class="conv_option" onclick="positiveFeedbackVA(\'' + interactionId + '\', \'' + botType + '\')">Ja</span>' +
                                '<span class="conv_option" onclick="negativeFeedbackVA(\'' + interactionId + '\', \'' + botType + '\')">Nee, ik wil graag contact</span>' +
                            '</p>';
                            lastItem.insertAdjacentHTML('beforeend', escalationHtml);
                            toBottom();
                        }
                    }
                }
                else {
                    let innerBubbles = bubbles;
                    innerBubbles.shift();
                    setTimeout(function () {
                        for (var i = 0; i < innerBubbles.length; i++) {
                            let bubble = innerBubbles[i];
                            (function(index) {
                                setTimeout(function() {
                                    items = document.querySelectorAll('.conv_message');
                                    lastItem = items[items.length- 1];
                                    lastItem.insertAdjacentHTML('beforeend', bubble);
                                    if (innerBubbles.length - 1 === index) {
                                        items = document.querySelectorAll('.conv_message');
                                        lastItem = items[items.length-1];
                                        paragraphs = lastItem.querySelectorAll('p');
                                        lastParagraph = paragraphs[paragraphs.length-1];
                                        if (lastParagraph.innerHTML.includes('Bent u zo geholpen?')) {
                                            lastParagraph.innerHTML = '';
                                        }
                                        console.log(data);

                                        if (data.outputs[0].dialogPath.includes('!')) {
                                            let botType = document.getElementById('bot').dataset.type;
                                            let escalationHtml = '<div><p>Bent u zo geholpen?</p></div><br />' +
                                            '<p class="conv_options" id="fulfillment">' +
                                                '<span class="conv_option" onclick="positiveFeedbackVA(\'' + interactionId + '\', \'' + botType + '\')">Ja</span>' +
                                                '<span class="conv_option" onclick="negativeFeedbackVA(\'' + interactionId + '\', \'' + botType + '\')">Nee, ik wil graag contact</span>' +
                                            '</p>';
                                            lastParagraph.insertAdjacentHTML('afterend', escalationHtml);
                                        }
                                        waitingElement.style.display = 'none';
                                        toBottom();
                                    }
                                    toBottom();
                                }, i * 1000);
                                toBottom();
                            })(i);
                        }
                    }, 1000);
                }
            }, 1000);
            localStorage.setItem("conversation", JSON.stringify(conversation));
        });
    });
}

function positiveFeedbackVA(interactionId, question) {
    const data = {
        "originInteractionId": interactionId,
        "score": 1,
        "label": question,
    }
    fetch('https://VVAA-VVAA.cxcompany.com/feedback?apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify(data)
    }).then((resp) => {return resp.text();
    }).then(function(data) {
        let thankYou = '<div class="conv_message final"><br />' +
                            '<p>Bedankt voor uw feedback!' +
                        '</div>'
        let fulfillment = document.getElementById('fulfillment');
        fulfillment.insertAdjacentHTML('afterend', thankYou);
        toBottom();
        let conv_options = fulfillment.getElementsByClassName('conv_option');
        conv_options[0].classList.add('noclick');
        conv_options[1].classList.add('noclick');
    });
}

function negativeFeedbackVA(interactionId, question) {
    const data = {
        "originInteractionId": interactionId,
        "score": -1,
        "label": question,
    }
    fetch('https://VVAA-VVAA.cxcompany.com/feedback?apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify(data)
    }).then((resp) => {return resp.text();
    }).then(function(data) {
        let botType = localStorage.getItem('bot');
        let chatHtml;
        if (botType === 'schade_melden_bot') {
            chatHtml = '<div class="conv_message final"><br />' +
                            '<p>Mijn collega helpt u graag telefonisch verder via <a href="tel:0302474886">030 247 48 86</a>.' +
                        '</div><br />';
        }
        if (botType === 'opleidingen_bot') {
            chatHtml = '<div class="conv_message final"><br />' +
                            '<p>Mijn collega helpt u graag telefonisch verder via <a href="tel:0302474886">030 247 48 86</a>.' +
                        '</div><br />';
        }
        else {
            chatHtml = '<div class="conv_message final"><br />' +
                            '<p>Mijn collega helpt u graag verder via de <u onclick="liveChat(\'' + 'VA' + '\')">chat</u>.' +
                        '</div><br />';
        }
        let fulfillment = document.getElementById('fulfillment');
        fulfillment.insertAdjacentHTML('afterend', chatHtml);
        toBottom();
        let conv_options = fulfillment.getElementsByClassName('conv_option');
        conv_options[0].classList.add('noclick');
        conv_options[1].classList.add('noclick');
    });
}

function revertChoice(dialogpath, el) {
    el.classList.toggle('active');
    let parentEl = el.closest('.conv_message');
    let optionsHtml = el.nextSibling;
    console.log(optionsHtml);
    let options = optionsHtml.querySelectorAll('.conv-option');
    console.log(options);
    options.forEach(function(el) {
        console.log(el);
        el.classList.remove('active');
    })
    let sessionId = localStorage.getItem("sessionid");
    fetch('https://VVAA-VVAA.digitalcx.com/dialogstep?path=' + dialogpath + '&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl&session.id='+sessionId)
    .then(response => { response.json().then(
        function(data) {
            console.log(data);
        });
    });
}

function submitText(e) {
    let textInput = document.getElementById('textinput');
    let inputValue = textInput.value;
    e.preventDefault();
    selectOption(inputValue);
    textInput.value = '';
}

// UTILS
function linksAndImagesVA(answer, interactionId) {
    if (answer.links.length) {
        for (var a = 0; a < answer.links.length; a++) {
            let iterator = a + 1;
            let link = answer.links[a];
            let url = link.url;
            let id = link.id;
            let linkHTML = '<a href="' + url + '" target="blank" data-id="' + link.id + '" data-interaction="' + interactionId + '" onclick="selectLink(this)">' + link.text + '</a>';
            let linkString = '%{Link(' + iterator + ')}';
            answer.text = answer.text.replace(linkString, linkHTML);
            if (answer.images.length) {
                for (var b = 0; b < answer.images.length; b++) {
                    let image = answer.images[b];
                    let iteratorImages = b + 1;
                    let imageHtml = '';
                    if (image.linkId) {
                        if (link.id === image.linkId) {
                            let imageHTML = '<a href="' + link.url + '" target="_blank"><img src="' + image.name + '" alt ="' + image.title + '"></img></a>';
                            let imageString = '%{Image(' + iteratorImages + ')}';
                            answer.text = answer.text.replace(imageString, imageHTML);
                        }
                    }
                    else {
                        let imageHTML = '<img src="' + image.name + '" alt ="' + image.title + '"></img>';
                        let imageString = '%{Image(' + iteratorImages + ')}';
                        answer.text = answer.text.replace(imageString, imageHTML);
                    }
                }
            }
        }
    }
}

function toBottom() {
    let bottom = document.getElementById("bot_body");
    bottom.scrollTop = bottom.scrollHeight;
}

function liveChat(type) {
    let bot = document.getElementById('bot');
    bot.classList.remove('active');
    if (navigator.userAgent.toUpperCase().indexOf("TRIDENT/") != -1 ||
    navigator.userAgent.toUpperCase().indexOf("MSIE") != -1 ||
    navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
        alert('De live chat wordt niet ondersteund door uw internetbrowser. Probeer het opnieuw in een andere browser.')
    }
    else {
        if (type === 'tree') {
            let category = localStorage.getItem('category');
            let subcategory = localStorage.getItem('subcategory');
            let faq = localStorage.getItem('faq');
            embedded_svc.settings.extraPrechatFormDetails = [{
                label: "FAQ Category",
                value: category,
                displayToAgent: true,
            },
            {
                label: "FAQ Subject",
                value: subcategory,
                displayToAgent: true,
            },
            {
                label: "FAQ Question",
                value: faq,
                displayToAgent: true,
            }];
        }
        if (type === 'search') {
            let searchFaq = localStorage.getItem('faq');
            let searchSubject = localStorage.getItem('searchsubject');
            if (searchSubject) {
                embedded_svc.settings.extraPrechatFormDetails = [
                {
                    label: "FAQ Subject",
                    value: searchSubject,
                    displayToAgent: true
                }];
            }
            if (searchFaq) {
                embedded_svc.settings.extraPrechatFormDetails = [
                {
                    label: "FAQ Question",
                    value: searchFaq,
                    displayToAgent: true
                }];
            }
        }
        if (type === 'VA') {
            let category = localStorage.getItem('category');
            let subcategory = localStorage.getItem('subcategory');
            let conversation = JSON.parse(localStorage.getItem("conversation"));
            let conversationNodes = '';
            for (var i = 0; i < conversation.length; i++) {
                let node = conversation[i];
                let conversationNode = node.text;
                conversationNode = conversationNode.split('<p>').join('');
                conversationNode = conversationNode.split('</p>').join('');
                if (node.type === 'answer') {
                    conversationNode = '<b>' + conversationNode + '</b>';
                }
                else {
                    conversationNode = '<u>' + conversationNode + '</u>';
                }
                conversationNodes += conversationNode;
            }
            embedded_svc.settings.extraPrechatFormDetails = [
                {
                    label: "FAQ Category",
                    value: category,
                    displayToAgent: true,
                },
                {
                    label: "FAQ Subject",
                    value: subcategory,
                    displayToAgent: true,
                },
                {
                    label:"VA Transcript",
                    value: conversationNodes,
                    transcriptFields:[ "VATranscript__c" ]
                }
            ];
        }
        embedded_svc.settings.extraPrechatInfo = [{
            entityFieldMaps: [{
                    doCreate: true,
                    doFind: false,
                    fieldName: "FAQCategory__c",
                    isExactMatch: true,
                    label: "FAQ Category"
                },
                {
                    doCreate: true,
                    doFind: false,
                    fieldName: "FAQSubject__c",
                    isExactMatch: true,
                    label: "FAQ Subject"
                },
                {
                    doCreate: true,
                    doFind: false,
                    fieldName: "FAQQuestion__c",
                    isExactMatch: true,
                    label: "FAQ Question"
                },
            ],
            entityName: "Case",
        }, ];
        embedded_svc.inviteAPI.inviteButton.acceptInvite();
    }
}
